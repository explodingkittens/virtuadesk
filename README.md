# VirtuaDesk: Productivity Suite for Virtual Reality
VirtuaDesk is an application built for SteamVR-compatible headsets that provides a set of APIs to allow building small productivity (or fun!) tools and applications for a virtual 3D environment.


## Features
* Dynamic desk area: When using a headset that fully supports SteamVR's Chaperone system, the bounds of the user's desk area will automatically bind to their play space. No setup required!
* Input abstraction: Instead of dealing with SteamVR directly, developers can get input information with a quick and simple API call.
* Input protection: To ensure user privacy and security, objects can only query for input when they're being held or directly interacted with.
* Physical data: Text, audio, and other forms of data can be moved from application to application in an intuitive take on drag-and-drop by representing data as a "physical" object.
* Notifications: Applications can send the user notifications containing data to get their attention. Developers can also read the status of a notification they've sent.


## Running VirtuaDesk
The VirtuaDesk project was last build on Unity 2019.2.19f. It may work on other versions of Unity, but don't count on it. To run the project, simply clone the repository, open the project in Unity, and you can run it from the editor. If you would like to make a build, you may also do that. VirtuaDesk has been tested as a standalone executable on Windows and works fine.


## Developing for VirtuaDesk
Our plan is to provide an SDK which developers can use to develop, package, and publish applications for VirtuaDesk. Since the project is still in early development, this is not yet possible, but you can develop a VirtuaDesk application directly in the Unity project if you're interested in looking around. Here's a quick guide for using the API!

### Terminology
* "Component" refers to a single physical object that the user will interact with (usually by picking them up).
* "Data object" refers to a physical object that represents data. This will have the `VD_DataObject` script attached to it, as described below.
* "Data facet" refers to a physical object that can receive data from a data object. This is usually a static object that cannot be picked up, but may be a child of an object that can be picked up and moved.
* "Application" refers to a set of components, data objects, and data facets that make up a cohesive application.


### Guidelines
1. Protect the user's privacy. Only access information you absolutely need in order to complete your tasks.
2. Don't reinvent the wheel. In most cases, the API simply provides the bare necessities. You can build up from there, but there's no need to reinvent things.
3. Make sure your data means something. When developing data objects, make sure they look distinct from other data objects-- even of the same time. You should be able to tell the difference between two pieces of data just by looking.
4. Be as specific as possible with data. When deciding on a data class to derive from, try to make your data as reducible as possible. For example, if you're creating a data type for an email address, consider deriving from Text instead of Data.


### Basic Component Awareness
The API provides a script that allows components to trigger events when they're thrown away.

#### `VD_Component`
This class contains a UnityEvent called `onThrownAway` that is triggered when the component is thrown away, and before it is officially destroyed.


### Input
The API provides a way for components to know when they've been picked up/dropped, and be able to access various points of data about the user's controller status.

#### `Grabbable`
This script should be applied to any object you expect to be picked up and grabbed. It should be accompanied by SteamVR's `Throwable` and `Interactable` scripts as well.

This class also contains the `grabbedBy` field, which allows you to see which hand is holding a particular component.

#### `VD_InputReceiver`
This class contains a UnityEvent called `grab` and a UnityEvent called `drop`. These events are invoked when the object is grabbed and dropped, respectively. Consequently, these events are invoked when the component gains and loses access to the user's input status, respectively.

This class also contains the `click` UnityEvent, which is invoked when the component is "clicked" with the trigger, but not currently held.

Finally, and most importantly, this class contains the `isActive` flag, and the `GetInput()` method. `isActive` will state if the component is being held and can therefore access data. `GetInput()` will return a `VD_InputsState` object with information about the user's current input status. Before calling `GetInput()`, always check that `isActive` is true, otherwise you will receive null data. See below for more details.

#### `VD_InputsState`
This class holds the trigger state and the touchpad state (mapped to the thumbstick on Oculus Touch and Index controllers). These states are stored as `triggerState` and `touchpadState`, respectively, and are `VD_InputTriggerState` and `VD_InputTouchpadState` objects respectively. See below for more.

#### `VD_InputTriggerState`
Contains information about the current state of the trigger:

* `value`: A value between 0 and 1 stating how far down the trigger is pulled.
* `clicked`: A boolean stating if the trigger is recognized as having been clicked down by SteamVR.
* `clickedDown`: A boolean stating if the trigger has been clicked down on this frame, but not on the previous frame. When the trigger is clicked, this value will be true for exactly 1 frame.
* `clickedUp`: A boolean stating if the trigger has been released on this frame, but was clicked on the previous frame. When the trigger is released, this value will be true for exactly 1 frame.

#### `VD_InputTouchpadState`
Contains information about the current state of the touchpad (Vive) or thumbsticks (Oculus/Index):

* `value`: A `Vector2` stating the current X and Y value reported on the touchpad or thumbstick.
* `clicked`: A boolean stating if the touchpad or thumbstick has been clicked down.
* `clickedDown`: A boolean stating if the touchpad or thumbstick has been clicked down on this frame, but was not clicked down on the previous frame.
* `clickedUp`: A boolean stating if the touchpad or thumbstick has been release on this frame, but was clicked down on the previous frame.
* `touched`: When supported, a boolean value stating if the touchpad or thumbstick is currently being touched by the user's finger.
* `touchedDown`: A boolean stating if the touchpad or thumbstick is currently being touched, but was not being touched on the previous frame.
* `touchedUp`: A boolean stating if the touchpad or thumbstick is not currently being touched, but was on the previous frame.


### Data
VirtuaDesk provides and interface for representing pieces of data as physical objects. You can create data objects to hold an instance of any class-- no matter how many fields it has. VirtuaDesk also provides some core classes.

#### `Data`
The base class for data. While you should generally try to inherit from one of Data's children if possible, you should always inherit from Data at the very least.

This class continues the `ToString()` chain. Whenever possible, always try to override `ToString()`.

#### `Text`
The base class for textual data. This simply has a string field called `data`, where you should store whatever text you wish to convey with this data.

This class also contains a `GetSnippet()` method. When deriving from Text, consider overriding the `GetSnippet()` method.

#### `Audio`
The base class for audio data. Unlike Text, this class has two fields: `clip`, an AudioClip, and `trackTitle`, a string.

This class also contains a `GetTrackName()` method. When deriving from Audio, consider overriding the `GetTrackName()` method.

#### `VD_DataObject`
This script should always be applied to a model that should represent data. This allows the data to be recognized by facets. When creating objects for data, this script should be bundled with `Grabbable`, `Throwable`, and `Interactable`. Additionally, you should always make your own script for behavior specific to your model, such as changing labels. This class provides some UnityEvents:

* `onSpawn` is invoked when the object is created. This should be used to populate any labels. If you can avoid it, please do not update labels in `Update()`.
* `onValidFacetHover` is invoked when the object comes in contact with a facet that will accept the data.
* `onValidFacetAway` is invoked when the object is no longer in contact with a valid facet.

If it is of any use to you, you can also access which valid facet the data object is currently contacting by looking at the `collidingFacet` field.

#### `VD_DataFacet`
This script should be applied to a model that should receive and/or hold data. Facets do not necessarily need to allow data to be dropped into them. This should never be the only script applied to a facet. You should always have a custom script for each facet type. This script contains the following fields:

* `accepts` defines the type of data accepted by the facet. You should set this in `Start()`.
* `data` contains any data already in the facet.
* `dataGrabbable` is a boolean stating whether the user can grab data out of the facet or not.
* `acceptingData` is a boolean stating whether the user can drop data into the facet or not.
* `grabType` is an enum stating if the data in the facet is either `Removable` (meaning the facet will be vacant when the data is pulled out), or `Cloneable` (meaning the facet will still have the data inside of it when the user pulls data out of it).
* `spawnObject` stores the prefab for the data object that should be spawned when the user pulls data out of the facet.

This script also has some events:

* `onValidDataHover` is invoked when valid data is hovered over the facet.
* `onInvalidDataHover` is invoked when data is hovered over the facet that the facet refuses to accept, for any reason.
* `onDataAway` is invoked when data is no longer hovering over the facet.
* `onDataDropped` is invoked when valid data is dropped and captured by the facet.
* `onDataClear` is invoked when the facet is vacated, therefore no longer containing any data.
* `onDataUpdate` is invoked when the data in the facet is changed. You should trigger this one in any code that might update the facet's data.


### Notifications
VirtuaDesk provides a way to send the user notifications. These notifications can contain data and a message of some form.

#### `VD_NotificationSender`
This script should be applied to any component you expect to send a notification. While there are no restrictions on how many components can have this script, you should generally try to centralize sending of notifications to one component, if possible.

Once applied to a component, this script contains a method called `CreateChannel(channelName, applicationName)` that must be called before you can send a notification. This will return a `VD_NotificationChannel` object, discussed below. The `channelName` argument should be a descriptive name that allows the user to understand what the channel is used for. The `applicationName` argument should contain the name for your application. There is no limit to how many channels you can have, but be reasonable.

Once you have created a channel, you can use it to create a notification with `CreateNotification(channel, subject, label, contents)`. This method will return a `VD_Notification` object, discussed below. The `channel` argument should be the appropriate `VD_NotificationChannel` object you wish to use for this notification. The `subject` string will be displayed on the outside of the notification, and should be short. The `label` string will be displayed after the notification has been opened, and can be a bit longer. `contents` is any `Data` object (or child object thereof) that should be sent with the notification.

#### `VD_NotificationChannel`
Objects of this class will be returned from `CreateChannel` being called on a `VD_NotificationSender` component. Channels allow you to more easily group different types of notifications, and allow the user more granular control over which types of notifications they wish to see from different applications.

#### `VD_Notification`
Objects of this class will be returned from `CreateNotification` being called on a `VD_NotificationSender` component. This will contain all of the data you submitted in the method call, as well as boolean values for whether the notification has been opened or destroyed: `opened` and `destroyed`, respectively. The date and time the notification was sent will also be present as `created`, a `DateTime` object.