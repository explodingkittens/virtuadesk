﻿using Data;
using UnityEngine;

namespace Input {
    /// <summary>
    /// Class to show that an object can be grabbed. This also holds what object has grabbed it.
    /// </summary>
    public class Grabbable : MonoBehaviour {
        public GrabControls grabbedBy;

        /// <summary>
        /// Called when the object has been grabbed. Sets the hand that has grabbed it.
        /// </summary>
        /// <param name="by"><c>GrabControls</c> representing which hand has grabbed the object.</param>
        public void Grabbed(GrabControls by) {
            grabbedBy = by;

            VD_DataObject dataObject = GetComponent<VD_DataObject>();
            if (dataObject != null) {
                dataObject.Grabbed(by);
            }
        }

        /// <summary>
        /// Called when dropped. Clears what hand has grabbed it.
        /// </summary>
        /// <param name="by"><c>GrabControls</c> representing which hand has dropped the object.</param>
        public void Dropped(GrabControls by) {
            if (by == grabbedBy) {
                VD_DataObject dataObject = GetComponent<VD_DataObject>();
                if (dataObject != null) {
                    dataObject.Dropped();
                }

                grabbedBy = null;
            }
        }
    }
}
