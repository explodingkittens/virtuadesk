﻿namespace Input {
	/// <summary>
	/// Data class holding the input states. This just contains the trigger and touchpad states.
	/// </summary>
	public class VD_InputsState {
		private VD_InputTriggerState triggerState;
		private VD_InputTouchpadState touchpadState;

		public VD_InputsState(VD_InputTriggerState triggerState, VD_InputTouchpadState touchpadState) {
			this.triggerState = triggerState;
			this.touchpadState = touchpadState;
		}

		public VD_InputTriggerState TriggerState => triggerState;

		public VD_InputTouchpadState TouchpadState => touchpadState;
	}
}