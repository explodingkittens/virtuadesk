﻿using UnityEngine;

namespace DialBoard {
    /// <summary>
    /// Behavior for the delete button on the DialBoard. Handles deleting the last character.
    /// </summary>
    public class DialBoardDeleteButton : MonoBehaviour {
        public DialBoardManager dialBoardManager;

        /// <summary>
        /// Event handler for being clicked. Calls the DialBoard manager and asks to delete the last character.
        /// </summary>
        public void Click() {
            dialBoardManager.Backspace();
        }
    }
}
