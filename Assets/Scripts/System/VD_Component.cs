﻿using UnityEngine;
using UnityEngine.Events;

namespace System {
	/// <summary>
	/// Script that can be attached to any component. When the trashcan sees this script attached to a component that is
	/// thrown away, it will invoke the <c>onThrownAway</c> event. This script is completely optional.
	/// </summary>
	public class VD_Component : MonoBehaviour {
		public UnityEvent onThrownAway;
	}
}
