﻿using UnityEngine;
using Valve.VR.InteractionSystem;

namespace System {
	/// <summary>
	/// Behavior for the trashcan. The trashcan is responsible for cleaning up the clutter. Any object thrown into the
	/// trashcan is deleted. If the object thrown into the trashcan is an "aware" component, the component is informed
	/// it has been thrown away.
	/// </summary>
	public class Trashcan : MonoBehaviour {
		/// <summary>
		///  Called when colliding with an object. If the object has a <c>Interactable</c> script attached, it will be
		/// deleted. If it also has a <c>VD_Component</c> script attached, the script's <c>onThrownAway</c> event will
		/// be triggered. 
		/// </summary>
		/// <param name="other">The collision that has occurred, which will contain a reference to the other object.
		/// </param>
		private void OnCollisionEnter(Collision other) {
			if (other.gameObject.GetComponent<Interactable>()) {
				VD_Component component = other.gameObject.GetComponent<VD_Component>();
				if (component != null) {
					component.onThrownAway.Invoke();
				}
			
				Destroy(other.gameObject);
			}
		}
	}
}
