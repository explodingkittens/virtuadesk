﻿using System.Collections.Generic;
using Notification;
using UnityEngine;

namespace System {
	/// <summary>
	/// System-level notification manager. This part of the notification manager is responsible for all of the guts of
	/// the operation. This will store channels and whether the channels are active (able to send notifications), and
	/// will actually spawn the notifications themselves.
	/// </summary>
	public class NotificationManager : MonoBehaviour {
		private Dictionary<VD_NotificationChannel, bool> channelStatuses;
		public GameObject notificationPrefab;

		/// <summary>
		/// Called on start. Just sets up the channel active map.
		/// </summary>
		private void Start() {
			channelStatuses = new Dictionary<VD_NotificationChannel, bool>();
		}

		/// <summary>
		/// Called by <c>VD_NotificationSender</c> objects. Sets up and registers a new, active channel, and returns the
		/// channel.
		/// </summary>
		/// <param name="channelName">The name of the channel.</param>
		/// <param name="applicationName">The name of the application.</param>
		/// <returns>The <c>VD_NotificationChannel</c> object that is created.</returns>
		public VD_NotificationChannel CreateChannel(string channelName, string applicationName) {
			VD_NotificationChannel newChannel = new VD_NotificationChannel(channelName, applicationName);
			channelStatuses.Add(newChannel, true);
			return newChannel;
		}

		/// <summary>
		/// Called by a <c>VD_NotificationSender</c> object. Sends a notification, if the channel the notification
		/// should be sent through is registered and still active. If so, a new notification object is spawned and the
		/// notification is returned. If not, null is returned. 
		/// </summary>
		/// <param name="channel">The channel the notification should be sent through.</param>
		/// <param name="subject">The subject of the notification.</param>
		/// <param name="label">The inner label of the notification.</param>
		/// <param name="contents">The data that should be sent with the notification.</param>
		/// <returns>A <c>VD_Notification</c> object, if sent. Otherwise, null.</returns>
		public VD_Notification SendNotification(VD_NotificationChannel channel, string subject, string label, global::Data.CoreData.Data contents) {
			// Ensure the channel is registered.
			if (!channelStatuses.ContainsKey(channel)) {
				Debug.LogWarning("Attempted to send a notification with channel \"" + channel.ChannelName + "\" that is not registered.");
				return null;
			}

			// Ensure the channel has not been disabled.
			if (!channelStatuses[channel]) {
				Debug.LogWarning("Attempted to send a notification with channel \"" + channel.ChannelName + "\" that has been disabled.");
				VD_Notification unsentNotification = new VD_Notification(label, subject, channel, contents);
				unsentNotification.sent = false;
				return unsentNotification;
			}
		
			// Create and spawn the notification.
			VD_Notification notification = new VD_Notification(label, subject, channel, contents);
			Transform mailboxLocation = FindObjectOfType<DeskManager>().mailbox;
			GameObject notificationObject = Instantiate(notificationPrefab);
			notificationObject.transform.position = mailboxLocation.position + new Vector3(0, 1f, 0);
			notificationObject.GetComponent<VD_NotificationObject>().Populate(notification);

			return notification;
		}

		/// <summary>
		/// This method will check with the map to tell if the specified channel is active. That is, if the channel can
		/// send notifications.
		/// </summary>
		/// <param name="channel">The channel to check.</param>
		/// <returns>Whether or not the channel can send notifications</returns>
		public bool IsActive(VD_NotificationChannel channel) {
			if (channelStatuses.ContainsKey(channel)) {
				return channelStatuses[channel];
			} else {
				return false;
			}
		}

		/// <summary>
		/// Toggle whether or not a particular channel is active. This will be called by the channel enablers/disablers
		/// on the universal menu.
		/// </summary>
		/// <param name="channel">The channel to toggle.</param>
		public void ToggleChannelActive(VD_NotificationChannel channel) {
			if (channelStatuses.ContainsKey(channel)) {
				channelStatuses[channel] = !channelStatuses[channel];
			}
		}

		/// <summary>
		/// Get a list of registered channels.
		/// </summary>
		/// <returns>A list of registered channels.</returns>
		public List<VD_NotificationChannel> GetRegisteredChannels() {
			return new List<VD_NotificationChannel>(channelStatuses.Keys);
		}
	}
}
