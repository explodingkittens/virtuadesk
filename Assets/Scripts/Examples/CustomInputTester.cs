﻿using Input;
using TMPro;
using UnityEngine;

namespace Examples {
	/// <summary>
	/// Behavior for the custom input tester. This example object simply tests that the individual data points for input
	/// are working correctly. The object will show the current location of the touchpad, and the time (in seconds since
	/// the start of the application) the trigger and touchpad were last touched, clicked, etc.
	/// </summary>
	public class CustomInputTester : MonoBehaviour {
		private VD_InputReceiver inputReceiver;

		private float curTime = 0.0f;

		public Transform touchpadIndicator;

		public Color grabbedColor;
		public Color droppedColor;
	
		public TMP_Text trigger;
		public TMP_Text touchpad;
		public TMP_Text touch;
	
		public TMP_Text lastTriggerDown;
		public TMP_Text lastTriggerUp;
	
		public TMP_Text lastTouchpadDown;
		public TMP_Text lastTouchpadUp;
	
		public TMP_Text lastTouchDown;
		public TMP_Text lastTouchUp;

		public TMP_Text lastClick;
	
		/// <summary>
		/// Called on start. Get our input receiver.
		/// </summary>
		private void Start() {
			inputReceiver = GetComponent<VD_InputReceiver>();
		}

		/// <summary>
		/// Called every frame. Get the input information that's available, and adjust the labels as necessary.
		/// </summary>
		private void Update() {
			curTime += Time.deltaTime;

			// We always need to check if the input receiver will get any data first.
			if (inputReceiver.isActive) {
				// Now, get our data for our trigger and touchpad state.
				VD_InputsState state = inputReceiver.GetInput();

				// If the trigger is currently clicked, set the trigger label to green. Otherwise, set it to red.
				if (state.TriggerState.Clicked) {
					trigger.color = Color.green;
				} else {
					trigger.color = Color.red;
				}

				// If the touchpad is clicked, set the touchpad label to green. Otherwise, set it to red.
				if (state.TouchpadState.Clicked) {
					touchpad.color = Color.green;
				} else {
					touchpad.color = Color.red;
				}

				// If the touchpad is touched, set the touchpad touch label to green. Otherwise, set it to red.
				if (state.TouchpadState.Touched) {
					touch.color = Color.green;
				} else {
					touch.color = Color.red;
				}

				// Set the last time our trigger was clicked down and released.
				if (state.TriggerState.ClickedDown) {
					lastTriggerDown.text = curTime.ToString("F1");
				} else if (state.TriggerState.ClickedUp) {
					lastTriggerUp.text = curTime.ToString("F1");
				}

				// Set the last time our touchpad was clicked down and released.
				if (state.TouchpadState.ClickDown) {
					lastTouchpadDown.text = curTime.ToString("F1");
				} else if (state.TouchpadState.ClickUp) {
					lastTouchpadUp.text = curTime.ToString("F1");
				}

				// Set the last time our touchpad was touched and released.
				if (state.TouchpadState.TouchDown) {
					lastTouchDown.text = curTime.ToString("F1");
				} else if (state.TouchpadState.TouchUp) {
					lastTouchUp.text = curTime.ToString("F1");
				}

				// Set the position of the touchpad value indicator.
				touchpadIndicator.localPosition = new Vector3(
					state.TouchpadState.Value.x,
					state.TriggerState.Value,
					state.TouchpadState.Value.y
				);
			}
		}

		/// <summary>
		/// Called when the object is picked up. When it is, set the cube to green.
		/// </summary>
		public void PickedUp() {
			GetComponent<Renderer>().material.color = grabbedColor;
		}

		/// <summary>
		/// Called when the object is dropped. When it is, set the cube to red.
		/// </summary>
		public void Dropped() {
			GetComponent<Renderer>().material.color = droppedColor;
		}

		/// <summary>
		/// Called when the cube is clicked. When it is, set the last clicked time.
		/// </summary>
		public void Clicked() {
			lastClick.text = curTime.ToString("F1");
		}
	}
}
