﻿using Data.CoreData;
using TMPro;
using UnityEngine;

namespace Data.CoreDataBehaviors {
    /// <summary>
    /// <c>AudioBehavior</c> defines the behaviors of the default audio data object. This object just has a single text
    /// label, which needs to have its data set on spawn.
    /// </summary>
    public class AudioBehavior : MonoBehaviour {
        public TMP_Text label;

        /// <summary>
        /// Event handler to be called on spawn. Should get the data from this object, and set the label text on the
        /// model.
        /// </summary>
        public void Spawn() {
            VD_DataObject data = GetComponent<VD_DataObject>();
            label.text = (data.data as Audio)?.trackTitle;
        }
    }
}
