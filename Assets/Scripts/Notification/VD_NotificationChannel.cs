﻿namespace Notification {
	/// <summary>
	/// Data class for notification channels. Simply holds data for the channels.
	/// </summary>
	public class VD_NotificationChannel {
		public VD_NotificationChannel(string channelName, string applicationName) {
			this.ChannelName = channelName;
			this.ApplicationName = applicationName;
		}

		public string ChannelName { get; }

		public string ApplicationName { get; }
	}
}
